# E3 IOC for testing purposes

## Modules

The startup script loads the standard ESS EPICS modules using the [e3-common](https://gitlab.esss.lu.se/e3-recipes/e3-common-recipe) package:

```
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
```

The **e3-common** package includes:
* epics-base
* require
* iocStats
* recsync
* caPutLog
* ess
* autosave
