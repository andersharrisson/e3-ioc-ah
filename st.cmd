# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables
epicsEnvSet("PREFIX", "IOC_AH")
epicsEnvSet("DEVICE", "Test IOC")
epicsEnvSet("LOCATION", "TEST")
epicsEnvSet("ENGINEER", "Anders Harrisson <anders.harrisson@ess.eu>"

# Load standard module startup scripts
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

cd $(E3_IOCSH_TOP)
dbLoadRecords("db/random.db", "P=$(PREFIX)")

# Call iocInit to start the IOC
iocInit()
